<?php
/**
*	author - Martin Maritim : trollmatt@gmail.com
*/

/**
*a simple form for sending SMS using the nexmo sms gateway.
*/

function nexmo_sms_form($form, &$form_state){
	$form = array();
	$form['from'] = array(
			'#type' => 'textfield',
			'#title' => t('From'),
			'#description' => t('who is sending the message eg MyCompany20'),
			'#required' => TRUE,
		);

	$form['recipient'] = array(
			'#type' => 'textfield',
			'#title' => t('To'),
			'#description' => t('The user you want to send the SMS to'),
			'#required' => TRUE,
		);

	$form['text'] = array(
			'#type' => 'textarea',
			'#title' => t('Message'),
			'#description' => t('The Message you want to send'),
			'#required' => TRUE,
		);

	$form['buttons']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Send SMS'),
		);
	$form['#theme'] = 'nexmo_sms_form';

	return $form;
}


/**
*	implements hook_theme()
*/

function nexmo_theme($existing, $type, $theme, $path){
	return array(
		'nexmo_sms_form' => array(
				'render element' => 'form',
				'template' => 'nexmo-sms-form',	
		),
	);
}

/**
 * Implements template_preprocess() to prepare variables for use inside the
 * nexmo-sms-form.tpl.php template file.
 */

function template_preprocess_nexmo_sms_form(&$variables){
	
	$variables['sms_form_content'] = array();
	$nexmo_form_hidden = array();

	  // Each form element is rendered and saved as a key in $sms_form_content, to
	  // give the themer the power to print each element independently in the
	  // template file.  Hidden form elements have no value in the theme, so they
	  // are grouped into a single element.

	foreach (element_children($variables['form']) as $key) {
	    $type = $variables['form'][$key]['#type'];
	    if ($type == 'hidden' || $type == 'token') {
	      $nexmo_form_hidden[] = drupal_render($variables['form'][$key]);
	    }
	    else {
	      $variables['sms_form_content'][$key] = drupal_render($variables['form'][$key]);
	    }
    }
    $variables['text_form_content']['hidden'] = implode($text_form_hidden);

	  // The entire form is then saved in the $sms_form variable, to make it easy
	  // for the themer to print the whole form.
	 $variables['sms_form'] = implode($variables['sms_form_content']);
}


//function to send a text message
function nexmo_sms_form_submit($form, &$form_state){
	//make sure $from is valid
	$from = $form_state['values']['from'];
	$message = $form_state['values']['text'];
	$to = $form_state['values']['recipient'];
	$unicode = null;
	if($unicode === null) {
			$containsUnicode = max(array_map('ord', str_split($message))) > 127;
		} else {
			$containsUnicode = (bool)$unicode;
	}

// URL Encode
	urlencode($from);
	urlencode($message);

// Send away!
	$post = array(
		'from' => $from,
		'to' => $to,
		'text' => $message,
		'type' => $containsUnicode ? 'unicode' : 'text'
	);

	if(sendRequest($post)){
		drupal_set_message('message sent successfully!');
	}
	return;

}

//validate user input
function nexmo_sms_form_validate($form, &$form_state){
	//Making sure strings are UTF-8 encoded
	$from = $form_state['values']['from'];
	if ( !is_numeric($from) && !mb_check_encoding($from, 'UTF-8') ) {
			form_set_error('from', t('From value needs to be a valid UTF-8 encoded string'));
			return false;
	}

	$message = $form_state['values']['text'];
	if ( !mb_check_encoding($message, 'UTF-8') ) {
			form_set_error('text', t('Message needs to be a valid UTF-8 encoded string'));
			return false;
	}

	// Make sure $from is valid
	validateOriginator($from);

}

/**
	 * Validate an originator string
	 *
	 * If the originator ('from' field) is invalid, some networks may reject the network
	 * whilst stinging you with the financial cost! While this cannot correct them, it
	 * will try its best to correctly format them.
	 */

function validateOriginator($inp){

	// Remove any invalid characters
		$ret = preg_replace('/[^a-zA-Z0-9]/', '', (string)$inp);

		if(preg_match('/[a-zA-Z]/', $inp)){
			// Alphanumeric format so make sure it's < 11 chars
			$ret = substr($ret, 0, 11);
		} else {

			// Numerical, remove any prepending '00'
			if(substr($ret, 0, 2) == '00'){
				$ret = substr($ret, 2);
				$ret = substr($ret, 0, 15);
			}
		}

		return (string)$ret;
}


/**
 * Prepare and send a new message.
 */

function sendRequest($data) {

	$ssl_verify = false; // Verify Nexmo SSL before sending any message

	//get the set api key and secret from nexmo configuration
	if(variable_get('nexmo_configuration_key')){
		$username = variable_get('nexmo_configuration_key');
	} else drupal_set_message('nexmo key not set.');

	if(variable_get('nexmo_configuration_secret')){
		$secret = variable_get('nexmo_configuration_secret');
	} else drupal_set_message('nexmo secret not set.');

	// Build the post data
	$data = array_merge($data, array('username' => $username, 'password' => $secret));
	$post = '';
	foreach($data as $k => $v){
		$post .= "&$k=$v";
	}

	// If available, use CURL
	if (function_exists('curl_version')) {
		if(variable_get('nexmo_configuration_base_rest_uri')){
			$nx_uri = variable_get('nexmo_configuration_base_rest_uri');
		}
		else drupal_set_message('nexmo SMS gateway rest url not set');

		$to_nexmo = curl_init($nx_uri);

		curl_setopt( $to_nexmo, CURLOPT_POST, true );
		curl_setopt( $to_nexmo, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $to_nexmo, CURLOPT_POSTFIELDS, $post );

		if ($ssl_verify === false) {
			curl_setopt( $to_nexmo, CURLOPT_SSL_VERIFYPEER, false);
		}

		$from_nexmo = curl_exec( $to_nexmo );
		curl_close ( $to_nexmo );

	} elseif (ini_get('allow_url_fopen')) {
		// No CURL available so try the awesome file_get_contents

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $post
			)
		);
		$context = stream_context_create($opts);
		$from_nexmo = file_get_contents($nx_uri, false, $context);

	} else {
		// No way of sending a HTTP post :(
		return false;
	}


	 return nexmoParse($from_nexmo);

}


/**
 * Parse server response.
 */
function nexmoParse ($from_nexmo) {

	$nexmo_response = '';

	$response = json_decode($from_nexmo);

	// Copy the response data into an object, removing any '-' characters from the key
	$response_obj = normaliseKeys($response);

	if ($response_obj) {
		$nexmo_response = $response_obj;

		// Find the total cost of this message
		$response_obj->cost = $total_cost = 0;
		if (is_array($response_obj->messages)) {
			foreach ($response_obj->messages as $msg) {
				$total_cost = $total_cost + (float)$msg->messageprice;
			}

			$response_obj->cost = $total_cost;
		}

		return $response_obj;

	} else {
		// A malformed response
		$nexmo_response = array();
		return false;
	}

}


/**
 * Recursively normalise any key names in an object, removing unwanted characters
 */
function normaliseKeys($obj) {
	// Determine if working with a class or array
	if ($obj instanceof stdClass) {
		$new_obj = new stdClass();
		$is_obj = true;
	} else {
		$new_obj = array();
		$is_obj = false;
	}


	foreach($obj as $key => $val){
		// If we come across another class/array, normalise it
		if ($val instanceof stdClass || is_array($val)) {
			$val = normaliseKeys($val);
		}

		// Replace any unwanted characters in they key name
		if ($is_obj) {
			$new_obj->{str_replace('-', '', $key)} = $val;
		} else {
			$new_obj[str_replace('-', '', $key)] = $val;
		}
	}

	return $new_obj;
}