<?php
/**
 * @file
 * Template file for the theming nexmo sms send form.
 *
 * Available custom variables:
 * - $sms_form: A string containing the pre-rendered form.
 * - $sms_form_content: An array of form elements keyed by the element name.
 *
 * Alternatively, you may print each form element in the order you desire,
 * adding any extra html markup you wish to decorate the form like this:
 *
 * <?php print $sms_form_content['element_name']; ?>
 *	
 *  author - Martin Maritim : trollmatt@gmail.com
 */
?>

<div class="container-inline">
	custom form!
	<?php  print $sms_form; ?>
</div>