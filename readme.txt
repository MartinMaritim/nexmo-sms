A Drupal module for sending SMS using the nexmo SMS gateway

You have to register for an account with nexmo( https://www.nexmo.com ) to use this module.
Navigate to /admin/config/nexmo/configuration and add your api key and secret from your nexmo account.

How to send an SMS

Navigate to /nexmo/sendsms and start sending. Please note that a link to the send sms form is provided in 
the navigation block so make sure its activated if you don't want to play with the address bar!

Additional options to come out soon!